﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaiCaoPickCardItem : BaseCardItem {

	[SerializeField]
	Button btnCard;
    [SerializeField]
    GameObject backCard;
	void Awake()
	{
		btnCard.onClick.AddListener (PickCardOnClick);
	}

	public void Init(int _index)
	{
		indexCard = _index;
        backCard.SetActive(true);
	}

	void PickCardOnClick()
	{
        backCard.SetActive(false);
        //GameObject.FindGameObjectWithTag ("GameController").SendMessage ("PickCard", indexCard);
    }
}
