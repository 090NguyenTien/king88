﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using DG.Tweening;

namespace ViewMB {
		
	public class MBCountDownManager : MonoBehaviour {

		[SerializeField]
		GameObject panel;
		[SerializeField]
				Image imgTime, imgLeft, imgRight;
		[SerializeField]
		Text txtTime1;

		onCallBack onComplete;

		Coroutine countDownThread;

		float timeCurrent, timeTotal;
		int part;

		bool isStart = false;

		void Update()
		{

			if (isStart) {
				int _time = (int)timeCurrent;
				timeCurrent -= Time.deltaTime;

				if (_time < 10) {
					if (_time > timeCurrent)
						SoundManager.PlaySound (SoundManager.COUNT_DOWN);
				}

				imgTime.fillAmount = timeCurrent / timeTotal;

				if (part != (int)timeCurrent) {
					part = (int)timeCurrent;
					ShowTime (part);
//										imgLeft.sprite = MBDataHelper.instance.GetSprNumber (part / 10);
//										imgRight.sprite = MBDataHelper.instance.GetSprNumber (part % 10);

				}

				if (timeCurrent < 0) {
					OnComplete ();
				}

			}
		}

		public void Show(int _timeTotal, onCallBack _onComplete = null)
		{
						SetEnable (true);
			imgTime.fillAmount = 1;
			imgTime.color = Color.green;
			timeTotal = timeCurrent = _timeTotal + 1;

			onComplete = _onComplete;

			isStart = true;
		}
		public void Show(int _timeCurrent, int _timeTotal, onCallBack _onComplete = null)
		{
						SetEnable (true);
			imgTime.fillAmount = _timeCurrent / _timeTotal;
			timeTotal = _timeTotal;
			timeCurrent = _timeCurrent;

			onComplete = _onComplete;

			isStart = true;
		}

		public void OnComplete()
		{
			isStart = false;

			if (countDownThread != null) {
				StopCoroutine (countDownThread);
				countDownThread = null;
			}

						SetEnable (false);
		}

		IEnumerator CountDownThread(int _second)
		{
			SetEnable (true);

			timeCurrent = timeTotal = _second;

			for (int i = _second; i > 0; i--) {
				ShowTime (i);
//								imgLeft.sprite = MBDataHelper.instance.GetSprNumber (i / 10);
//								imgRight.sprite = MBDataHelper.instance.GetSprNumber (i % 10);

				yield return new WaitForSeconds (1);
			}

			countDownThread = null;
			onComplete ();

			SetEnable (false);

		}

		public void SetEnable(bool _enable)
		{
			panel.SetActive (_enable);
			txtTime1.gameObject.SetActive (_enable);
//						imgLeft.color = new Color (1, 1, 1, _enable ? 1 : 0);
//						imgRight.color = new Color (1, 1, 1, _enable ? 1 : 0);
		}

		void ShowTime(int _second)
		{
			txtTime1.text = _second.ToString ();
//						imgLeft.sprite = MBDataHelper.instance.GetSprNumber (_second / 10);
//						imgRight.sprite = MBDataHelper.instance.GetSprNumber (_second % 10);
		}

	}
}