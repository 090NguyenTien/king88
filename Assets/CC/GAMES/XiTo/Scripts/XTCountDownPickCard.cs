﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class XTCountDownPickCard : MonoBehaviour {


		[SerializeField]
		GameObject panel;
		[SerializeField]
		Slider imgTime;

		onCallBack onComplete;

		Coroutine countDownThread;

		float timeCurrent, timeTotal;

		bool isStart = false;

		void Update()
		{

				if (isStart) {
						timeCurrent -= Time.deltaTime;

						imgTime.value = timeCurrent / timeTotal;

						if (timeCurrent < 0) {
								OnComplete ();
						}
				}
		}

		public void Show(int _timeTotal, onCallBack _onComplete = null)
		{
				panel.SetActive(true);
				imgTime.value = 1;
//				imgTime.color = Color.green;
				timeTotal = timeCurrent = _timeTotal + 1;

				onComplete = _onComplete;

				isStart = true;
		}
		public void Show(int _timeCurrent, int _timeTotal, onCallBack _onComplete = null)
		{
				panel.SetActive(true);
				imgTime.value = _timeCurrent / _timeTotal;
				timeTotal = _timeTotal;
				timeCurrent = _timeCurrent;

				onComplete = _onComplete;

				isStart = true;
		}

		public void OnComplete()
		{
				isStart = false;

				if (countDownThread != null) {
						StopCoroutine (countDownThread);
						countDownThread = null;
				}

//				panel.SetActive (false);
		}
}
