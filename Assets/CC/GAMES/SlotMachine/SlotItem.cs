﻿using BaseCallBack;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotItem : MonoBehaviour {
    [SerializeField]
    XTCardItem card;
    [SerializeField]
    GameObject cardAnimation, EndPosAnimation;
    [SerializeField]
    List<XTCardItem> lstCardAnimation;
    //[SerializeField]
    //int slotId;
	// Use this for initialization
	void Start () {
        cardAnimation.gameObject.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public XTCardItem getCard()
    {
        return card;
    }
    public List<XTCardItem> getListCardAnimation()
    {
        return lstCardAnimation;
    }
    
    public IEnumerator runAnimationSlot(int index, onCallBack cb=null)
    {
        yield return new WaitForSeconds(float.Parse(index.ToString()) * 0.1f);        
        //Debug.LogError("runAnimationSlot===============");
        cardAnimation.gameObject.SetActive(true);
        cardAnimation.transform.DOMove(EndPosAnimation.transform.position, 2.5f, false).SetEase(Ease.InOutExpo);
        yield return new WaitForSeconds(2.5f);
        cardAnimation.gameObject.SetActive(false);
        cardAnimation.transform.localPosition = new Vector3(0,0,0);
        if(cb!=null) cb();
    }
}
