﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhomDeck : MonoBehaviour {

    public PhomCard CardObject;
    public List<PhomCard> Cards;
    public Sprite[] ValueSprites;
    public Sprite[] SmallTypeSprites;
    public Sprite[] BigTypeSprites;
    public Sprite[] JqkSprites;

    void Awake()
    {
        Cards = new List<PhomCard>();
        for (int i = 1; i <= 4; i++)
        {
            for (int j = 1; j <= 13; j++)
            {
                int index = (i - 1) * 13 + (j - 1);
                PhomCard card = Instantiate(CardObject);
                card.transform.SetParent(transform, false);
                card.Id = index;
                card.Value = j;
                card.Type = (CardType)(i - 1);

                if ((int)card.Type < 2)
                    card.ValueImg.color = Color.black;
                else
                    card.ValueImg.color = Color.red;

                card.ValueImg.sprite = ValueSprites[j - 1];
                //card.ValueImg.SetNativeSize();

                card.SmallTypeImg.sprite = SmallTypeSprites[i - 1];
                //card.SmallTypeImg.SetNativeSize();

                if (j >= 11)
                    card.BigTypeImg.sprite = JqkSprites[j - 11];
                else
                    card.BigTypeImg.sprite = BigTypeSprites[i - 1];

                //card.BigTypeImg.SetNativeSize();
                card.gameObject.SetActive(true);

                Cards.Add(card);
            }
        }
    }
}