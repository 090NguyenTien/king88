﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TournamentAlert : MonoBehaviour
{
    [SerializeField]
    private Button btnTouchOut = null, btnJoin = null;

    private Action<long> actionJoinRoom;

    [SerializeField]
    private Transform bettingContainer = null;

    [SerializeField]
    private Toggle prefabTogBetting = null;
    

    private long curMoneyBetSelected = 0;
    private List<Toggle> lstTogItem = new List<Toggle>();
    public void showAlert(Action<long> moneyBet)
    {
        actionJoinRoom = moneyBet;
        btnJoin.onClick.AddListener(onJoinEventClick);

        for (int i = 0; i < bettingContainer.childCount; i++)
        {
            Destroy(bettingContainer.GetChild(i).gameObject);
        }

        for (int i = 0; i < MyInfo.tournamentBettingArr.Length; i++)
        {
            Toggle togItem = Instantiate(prefabTogBetting, bettingContainer);
            togItem.GetComponentInChildren<Text>().text = Utilities.GetStringMoneyByLong(MyInfo.tournamentBettingArr[i]); 
            lstTogItem.Add(togItem);
        }
        btnTouchOut.onClick.AddListener(closeAlert);
        gameObject.SetActive(true);
    }

    private void onJoinEventClick()
    {
        actionJoinRoom(curMoneyBetSelected);
        closeAlert();
    }

    private void closeAlert()
    {
        btnJoin.onClick.RemoveAllListeners();
        btnTouchOut.onClick.RemoveAllListeners();
        gameObject.SetActive(false);
    }
}
