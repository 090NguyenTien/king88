﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TournamentType
{
    WinCount = 1,
    MoneyWin = 2
}
public class TournamentWaitRoom : MonoBehaviour
{
    [SerializeField]
    private Button btnExit = null, btnShowAlert = null;

    [SerializeField]
    private Text txtGameHeader = null;

    [SerializeField]
    private TournamentAlert tournamentAlert = null;

    [Header("LeaderBoard")]
    [SerializeField]
    private Text txtValueType = null;

    [SerializeField]
    private TournamentLeaderBoardItem prefabLeaderboardItem = null;

    [SerializeField]
    private Transform leaderboardContainer = null;

    [SerializeField]
    private Toggle togDay = null, togWeek = null;

    [SerializeField]
    private Text txtMyRank = null, txtMyValue = null;



    private long myValue = 0;

    private enum LeaderBoardType
    {
        Day = 1,
        Week = 2
    }

    private LeaderBoardType curLeaderBoardType;

    private TournamentStatus curWaitRoomType = TournamentStatus.None;
    public void showWaitRoom(GamePacket gamePackage)
    {
        if (curWaitRoomType != MyInfo.tournamentStatus)
        {
            dicLeaderBoardData.Clear();
            dicMyValue.Clear();
            lastTimeGetLeaderBoard = -500;
        }

        string gameName = "";
        switch (MyInfo.tournamentStatus)
        {
            case TournamentStatus.None:
                break;
            case TournamentStatus.XT_Tournament:
                gameName = "Xì Tố";
                break;
            case TournamentStatus.MB_Tournament:
                gameName = "Mậu Binh";
                break;
            case TournamentStatus.TaiXiu_Tournament:
                gameName = "Tài Xỉu";
                break;
            case TournamentStatus.Phom_Tournament:
                gameName = "Phỏm";
                break;
            case TournamentStatus.TLMN_Tournament:
                gameName = "Tiến Lên Miền Nam";
                break;
            case TournamentStatus.TLDL_Tournament:
                gameName = "Tiến Lên Đếm Lá";
                break;
            default:
                break;
        }
        txtGameHeader.text = gameName;
        curWaitRoomType = MyInfo.tournamentStatus;

        MyInfo.tournamentType = (TournamentType)gamePackage.GetInt(ParamKey.TYPE);
        if (MyInfo.tournamentType == TournamentType.MoneyWin)
        {
            MyInfo.tournamentBettingArr = gamePackage.GetLongArray(ParamKey.BETTING_MONEY);
            txtValueType.text = "Tiền thắng";
        }
        else
        {
            txtValueType.text = "Trận Thắng";
        }

        myValue = gamePackage.GetLong(ParamKey.MINE);

        btnShowAlert.onClick.AddListener(joinEvent);
        btnExit.onClick.AddListener(requestExitRoomEvent);

        togDay.onValueChanged.RemoveAllListeners();
        togWeek.onValueChanged.RemoveAllListeners();

        togDay.isOn = true;
        togWeek.isOn = false;
        curLeaderBoardType = LeaderBoardType.Day;

        togDay.onValueChanged.AddListener(onSelectDayLeaderBoard);
        togWeek.onValueChanged.AddListener(onSelectWeekLeaderBoard);

        callGetLeaderBoard(LeaderBoardType.Day);
        gameObject.SetActive(true);
    }
    private Dictionary<LeaderBoardType, ISFSArray> dicLeaderBoardData = new Dictionary<LeaderBoardType, ISFSArray>();
    private Dictionary<LeaderBoardType, long> dicMyValue = new Dictionary<LeaderBoardType, long>();
    private Dictionary<LeaderBoardType, int> dicMyRank = new Dictionary<LeaderBoardType, int>();
    public void onLeaderBoardResponse(GamePacket leaderBoardData)
    {
        LeaderBoardType leaderBoardType = (LeaderBoardType)leaderBoardData.GetInt("type");
        dicLeaderBoardData[leaderBoardType] = leaderBoardData.GetSFSArray("leaderboard");
        dicMyValue[leaderBoardType] = leaderBoardData.GetLong("mine");
        dicMyRank[leaderBoardType] = leaderBoardData.GetInt("mine_rank");

        showLeaderBoard(leaderBoardType);
    }

    private void showLeaderBoard(LeaderBoardType leaderBoardType)
    {
        for (int i = 0; i < leaderboardContainer.childCount; i++)
        {
            Destroy(leaderboardContainer.GetChild(i).gameObject);
        }

        ISFSArray leaderBoardData = dicLeaderBoardData[leaderBoardType];
        for (int i = 0; i < leaderBoardData.Count; i++)
        {
            TournamentLeaderBoardItem item = Instantiate(prefabLeaderboardItem, leaderboardContainer);
            item.initLeaderboardItem(i + 1, leaderBoardData.GetSFSObject(i), MyInfo.tournamentType);
        }

        if (dicMyRank[leaderBoardType] <= 0)
        {
            txtMyRank.text = "Hạng: --- ";
        }
        else
        {
            txtMyRank.text = "Hạng: " + dicMyRank[leaderBoardType].ToString();
        }
        
        if (MyInfo.tournamentType == TournamentType.WinCount)
        {
            txtMyValue.text = "Bàn thắng: " + dicMyValue[leaderBoardType].ToString();
        }
        else
        {
            txtMyValue.text = "Tiền thắng: " + Utilities.GetStringMoneyByLong(dicMyValue[leaderBoardType]);
        }
       
    }
    private void onSelectDayLeaderBoard(bool isEneble)
    {
        if (isEneble == true)
        {
            callGetLeaderBoard(LeaderBoardType.Day);
        }
    }
    private void onSelectWeekLeaderBoard(bool isEneble)
    {
        if (isEneble == true)
        {
            callGetLeaderBoard(LeaderBoardType.Week);
        }
    }

    private float lastTimeGetLeaderBoard = -500;
    private bool isGetWeekRanking = false;
    private void callGetLeaderBoard(LeaderBoardType leaderBoardType)
    {
        if (leaderBoardType == LeaderBoardType.Day)
        {
            if (Time.realtimeSinceStartup - lastTimeGetLeaderBoard < 300)//chua du 5 phut
            {
                return;
            }
        }
        else
        {
            if (isGetWeekRanking == true)
            {
                return;
            }
        }
       
        lastTimeGetLeaderBoard = Time.realtimeSinceStartup;
        GamePacket gamePackage = new GamePacket(CommandKey.LEADERBOARD);
        gamePackage.Put("top_type", (int)leaderBoardType);
        SFS.Instance.SendRoomRequest(gamePackage);
    }

    private void joinEvent()
    {
        btnShowAlert.onClick.RemoveAllListeners();
        if (MyInfo.tournamentType == TournamentType.WinCount)
        {
            callStartEvent();
        }
        else if (MyInfo.tournamentType == TournamentType.MoneyWin)
        {
            if (MyInfo.tournamentStatus == TournamentStatus.TaiXiu_Tournament)
            {
                callStartEvent();
            }
            else
            {
                tournamentAlert.showAlert(callStartEvent);
            }
        }
    }

    public void callStartEvent(long moneyBet = 0)
    {
        GamePacket gamePackage = new GamePacket(CommandKey.START_GAME_EVENT);
        if (MyInfo.tournamentType == TournamentType.MoneyWin)
        {
            gamePackage.Put("max_bet_money", moneyBet);
        }
        SFS.Instance.SendRoomRequest(gamePackage);
    }

    private void requestExitRoomEvent()
    {
        GamePacket gamePackage = new GamePacket(CommandKey.EXIT_GAME_EVENT);
        SFS.Instance.SendRoomRequest(gamePackage);

        btnShowAlert.onClick.RemoveAllListeners();
        btnExit.onClick.RemoveAllListeners();
        togDay.onValueChanged.RemoveAllListeners();
        togWeek.onValueChanged.RemoveAllListeners();
    }
    public void closeTournamentWaitRoom()
    {
        btnShowAlert.onClick.RemoveAllListeners();
        btnExit.onClick.RemoveAllListeners();
        togDay.onValueChanged.RemoveAllListeners();
        togWeek.onValueChanged.RemoveAllListeners();
        curWaitRoomType = TournamentStatus.None;
        gameObject.SetActive(false);
    }
}
