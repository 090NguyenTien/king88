﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;
using UnityEngine.SceneManagement;
using SimpleJSON;

public class PopupInfoEnemy : MonoBehaviour
{
    [SerializeField]
    GameObject BtnKetBan, TxtStatusFriend;
    [SerializeField]
    Image Avatar, KhungAvatar;
    [SerializeField]
    Text TxtTen, TxtLoaiVip, TxtLevel, TxtChip, TxtGem;
    [SerializeField]
    List<Sprite> ListSprBoom;
    [SerializeField]
    Transform ChuaItemsBoom;
    int Enemy;
    onCallBackInt onClick;
    [SerializeField]
    GameObject Popup;
    [SerializeField]
    ControllerMB.MBInGameController MauBinhControll;
    [SerializeField]
    ControllerXT.XTInGameController XiToControll;

    [SerializeField]
    TLMNRoomController TienLenControll;
    public static PopupInfoEnemy Instance { get; set; }

    int sfsId_MB;
    string userId;

    public string MadUser = "1,2,3,4,5,6,3,2";

    [SerializeField]
    MadControll Mad;

    // Use this for initialization
    public void Init(string _userId, Sprite _ava, Sprite _KhungAva, string _ten, string _vip, string _level, string _chip, string _gem, int _enemy, int sfsid = -2)
    {
        Debug.Log("Vo Toi Init");
        if (MyInfo.CHAT_STATUS == 0)
        {
            BtnKetBan.SetActive(false);
        }
        else
        {
            BtnKetBan.SetActive(true);
        }
        this.userId = _userId;
        if (_ava != null)
        {
            Avatar.sprite = _ava;
        }

        if (_KhungAva != null)
        {
            KhungAvatar.sprite = _KhungAva;
        }
        TxtTen.text = _ten;
        TxtLoaiVip.text = "Vip " + _vip;
        TxtLevel.text = "Level " + _level;
        TxtChip.text = _chip;
        TxtGem.text = _gem;

        Enemy = _enemy;

        sfsId_MB = sfsid;

        int MyVip = MyInfo.MY_ID_VIP;

        for (int i = 0; i < ChuaItemsBoom.childCount; i++)
        {

            // ChuaItemsBoom.GetChild(i).GetComponent<Button>().onClick.AddListener(ItemBoomOnClick);
            int VipUnClock = DataHelper.DicItemBoom[i];
            GameObject Boom = ChuaItemsBoom.GetChild(i).gameObject;
            if (MyVip >= VipUnClock)
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
            }
            else
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomClockOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
                Boom.GetComponent<Image>().color = Color.gray;
                Text TxtVip = Boom.transform.GetChild(0).gameObject.GetComponent<Text>();
                TxtVip.text = "Vip " + VipUnClock.ToString();
                TxtVip.gameObject.SetActive(true);
            }


        }

        Popup.SetActive(true);
        //Check đã là bạn chưa
        CheckIsFriend();
        API.Instance.RequestVatPham_SoHuu_Enemy(_userId, RspLayDuLieuVatPham_SoHuu);

    }
    void CheckIsFriend()
    {

    }
    public void AddFriend()
    {
        BtnKetBan.SetActive(false);
        TxtStatusFriend.GetComponent<Text>().text = "Đang Chờ Đồng Ý";
        SFS.Instance.RequestAddFriend(userId);
    }
    Sprite GetSprite(int _index)
    {
        return ListSprBoom[_index];
    }


    public void ItemBoomClockOnClick(int id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        // Popup.SetActive(false);
    }

    public void ItemBoomOnClick(int id)
    {

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);


        if (SceneManager.GetActiveScene().name.Equals(GameScene.TLMNScene.ToString()))
        {

            TienLenControll.NemBoomControll(0, Enemy, id);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.PhomScene.ToString()))
        {
            PhomRoomController.Instance.NemBoomControll(0, Enemy, id);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.MBScene.ToString()))
        {
            MauBinhControll.NemBoomControll(0, Enemy, id, sfsId_MB);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.XTScene.ToString()))
        {
            XiToControll.NemBoomControll(0, Enemy, id);
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.BaiCaoScence.ToString()))
        {
            BaiCaoRoomController.Instance.NemBoomControll(0, Enemy, id);
        }



    }

    public void ClosePopup()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);
    }




    public void RequestLayDuLieuVatPham_SoHuu()
    {


    }

    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("DU LIEU SAN PHAM so huu: " + _json);
        DataHelper.DuLieuMap_Enemy.Clear();
        if (_json == "false")
        {
            //  Debug.LogError("DU LIEU SAN PHAM so huu------------------------:");
            DataHelper.DuLieuMap_Enemy.Add(0, "5dc03420e52b8e2d238b6045");
            for (int i = 1; i < 8; i++)
            {
                DataHelper.DuLieuMap_Enemy.Add(i, "");
            }
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);

            //   Debug.LogWarning("000000000000000000000000v       " + node["placed_assets"].Value);
            // string dataMap = node["placed_assets"].Value;
            if (node["placed_assets"].Value == "null")
            {

                DataHelper.DuLieuMap_Enemy.Add(0, "5dc03420e52b8e2d238b6045");
                for (int i = 1; i < 8; i++)
                {
                    DataHelper.DuLieuMap_Enemy.Add(i, "");
                }

                //   Debug.LogError("BAT DC NE HEHE---------- ");
            }
            else
            {
                int cou = node["placed_assets"].Count;
                // Debug.LogError("BAT DC so luong NE HEHE---------- " + cou);

                for (int i = 0; i < cou; i++)
                {
                    int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                    string a_id = node["placed_assets"][i]["id"].Value;

                    //  Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                    DataHelper.DuLieuMap_Enemy[pos] = a_id;
                }

                for (int i = 0; i < 8; i++)
                {
                    if (DataHelper.DuLieuMap_Enemy.ContainsKey(i) == false)
                    {
                        DataHelper.DuLieuMap_Enemy.Add(i, "");
                    }

                    //   Debug.LogError("ket vi tri ----- " + i  + " ---------- " + DataHelper.DuLieuMap[i]);
                }

            }
        }


        Mad.XemNha();

    }


}
