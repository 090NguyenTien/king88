﻿using UnityEngine;
using System.Collections;
using BaseCallBack;
using UnityEngine.UI;
using SimpleJSON;

public class PopupArenaManager : MonoBehaviour {

	[SerializeField]
	Image imgName;
	[SerializeField]
	Button btnBack, btnRegister, btnStart, btnFinish;
	[SerializeField]
	Text txtTimeRemain, txtUserCount, txtStatus;
	[SerializeField]
	Toggle toggleDaily, toggleWeekly;

	[SerializeField]
	Transform trsfAchivParent;

	[SerializeField]
	GameObject panel, objItemTop, popupMatching;

	JSONNode nodeTopDaily, nodeTopWeekly;

	bool IsStarted;

	float timeRemain;

	public long COST{ get; set; }
	onCallBack backClick, registerClick, startClick, finishClick;

	SFS sfs{
		get{ return SFS.Instance; }
	}

	public void Init(onCallBack _onBackClick, onCallBack _regisClick, onCallBack _startClick, onCallBack _finishClick){

		btnBack.onClick.AddListener (BtnBackOnClick);

		backClick = _onBackClick;

		registerClick = _regisClick;
		startClick = _startClick;
		finishClick = _finishClick;

		btnRegister.onClick.AddListener (BtnRegisterOnClick);
		btnStart.onClick.AddListener (BtnStartOnClick);
		btnFinish.onClick.AddListener (BtnFinishOnClick);

		toggleDaily.onValueChanged.AddListener (OnToggleChange);
		toggleWeekly.onValueChanged.AddListener (OnToggleChange);

	}

	void Update(){

		if (timeRemain < 1) {
			ShowTimeRemain (0);
			return;
		}
		
		timeRemain -= Time.deltaTime;

		ShowTimeRemain (timeRemain);

		if (timeRemain < 0) {
			IsStarted = false;

			registerClick ();
		}
	}

	public void ShowName(Sprite _spr){
		imgName.sprite = _spr;
	}

	#region STATUS

	public void ShowStatus(int _status){
		if (_status == 0)
			txtStatus.text = "Thi đấu sau:";
		else {
			txtStatus.text = "Thi đấu kết thúc sau:";
		}
	}

	#endregion

	#region USER COUNT

	public void ShowUserCount(int _count){
		txtUserCount.text = 
			"Số người tham gia: <color=yellow>" + _count.ToString() + "</color>";

	}

	#endregion

	#region Panel RIGHT - ACHIVEMENT

	public void InitTopAchivement(JSONNode _nodeDaily, JSONNode _nodeWeekly){

		nodeTopDaily = _nodeDaily;
		nodeTopWeekly = _nodeWeekly;
		Debug.Log ("=======\n" + nodeTopDaily.Value);
		Debug.Log ("=======\n" + nodeTopDaily);
		Debug.Log ("=======\n" + nodeTopWeekly.Value);
		Debug.Log ("=======\n" + nodeTopWeekly);
	}

	void ShowTopAchivement(JSONNode _node){

		int _count = 0;

		for (int i = 0; i < _node.Count; i++) {

			ItemTopArenaView itemView;

			if (i < trsfAchivParent.childCount) {

				itemView = trsfAchivParent.GetChild (i).GetComponent<ItemTopArenaView> ();

			} else {


				GameObject newitem = Instantiate (objItemTop) as GameObject;
				newitem.transform.SetParent (trsfAchivParent);
				newitem.transform.localScale = Vector3.one;

				itemView = newitem.GetComponent<ItemTopArenaView> ();

			}


			itemView.Show (i + 1, _node [i] ["username"].Value, 
				Utilities.GetStringMoneyByLong (long.Parse (_node [i] ["chip"].Value)));

			_count++;
		}

		for (int j = _count; j < trsfAchivParent.childCount; j++) {
			Destroy (trsfAchivParent.GetChild (j).gameObject);
		}
	}

	public void ShowAchivement(){
		OnToggleChange (true);
	}

	void OnToggleChange (bool _enable)
	{
		if (!_enable)
			return;

		if (toggleDaily.isOn)
			ShowTopAchivement (nodeTopDaily);
		else if (toggleWeekly.isOn)
			ShowTopAchivement (nodeTopWeekly);
	}

	#endregion

	#region POPUP MATCHING

	public void ShowPopupMatching(){

		popupMatching.SetActive (true);

	}
	public void HidePopupMatching(){
		popupMatching.SetActive (false);
	}

	#endregion

	public void ShowRemainTime(int _secondRemain){
		timeRemain = _secondRemain;
	}

	public void SetEnableBtnRegister(bool _enable){
		btnRegister.gameObject.SetActive (_enable);
	}
	public void SetEnableBtnStart(bool _enable, bool _isActive = true){
		btnStart.gameObject.SetActive (_enable);
		btnStart.interactable = _isActive;
	}
	void ShowTimeRemain (float _second)
	{
		int second = (int)_second % 60;
		int minute = ((int)_second / 60) % 60;
		int hour = ((int)_second / 3600);

		string sTime = "";

			sTime = hour.ToString ("D2") + ":";
			sTime += minute.ToString ("D2") + ":";
			sTime += second.ToString ("D2");


		txtTimeRemain.text = sTime;
	}

	void BtnRegisterOnClick ()
	{
		registerClick ();
	}

	void BtnStartOnClick ()
	{
		startClick ();	
	}

	void BtnFinishOnClick ()
	{
		
	}


	void BtnBackOnClick ()
	{
		backClick ();
	}

	public void Show(){
		
		panel.SetActive (true);

	}
	public void Hide(){
		
		panel.SetActive (false);

	}


}
