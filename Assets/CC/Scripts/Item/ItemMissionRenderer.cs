﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Entities.Data;
using System;

public class ItemMissionRenderer : MonoBehaviour {

    [SerializeField]
    Button btnClaim;
    [SerializeField]
    Text TxtMissionName, TxtRequired, TxtGold, TxtCircle, TxtComplete;
    [SerializeField]
    Image ImgGold, ImgCircleRaw;

    private int MissionChip;
    private SFS sfs
    {
        get { return SFS.Instance; }
    }

    public string Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    SFSObject dataMission;
    private string id;
    //private GameObject panelPopupMissionManager;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Init(SFSObject dataMission)
    {
        //Debug.LogError("ItemMissionRenderer:::dataMission====" + dataMission.ToJson());
        this.gameObject.SetActive(true);
        this.dataMission = dataMission;
        
        Id = dataMission.GetUtfString("id");
        int current = dataMission.GetInt("current");
        int require = dataMission.GetInt("require");
        TxtMissionName.text = dataMission.GetUtfString("name");
        TxtRequired.text = current + "/" + require;
        SFSObject gift = (SFSObject) dataMission.GetSFSObject("gift");
        TxtComplete.enabled = false;
        //set chip
        if (gift.GetInt("chip") != 0)
        {
            ImgGold.enabled = true;
            TxtGold.text = gift.GetInt("chip").ToString();
            MissionChip = gift.GetInt("chip");
        }
        else
        {
            ImgGold.enabled = false;
            TxtGold.text = "";
            MissionChip = 0;
        }
        //set vong quay
        if (gift.GetInt("circle") != 0)
        {
            ImgCircleRaw.enabled = true;
            TxtCircle.text = gift.GetInt("circle").ToString();
        }
        else
        {
            ImgCircleRaw.enabled = false;
            TxtCircle.text = "";
        }
        //check enable Claim
        int status = dataMission.GetInt("status");
        if (status == 0)
        {            
            int type = dataMission.GetInt("type");
            if (type == 1)
            {            
                activeBtnClaim();
            }
            else
            {             
                btnClaim.gameObject.SetActive(false);
            }
        }else if(status == 1)
        {
            activeBtnClaim();            
        }else if(status == 2)
        {
            btnClaim.gameObject.SetActive(false);
            TxtComplete.enabled = true;
        }
        else
        {
            btnClaim.gameObject.SetActive(false);
        }
        
    }
    void activeBtnClaim()
    {        
        btnClaim.gameObject.SetActive(true);
        btnClaim.onClick.AddListener(onClickClaimHandler);
    }
    private void onClickClaimHandler()
    {
       
        Debug.Log("ItemMissionRenderer:::type====" + dataMission.GetInt("type"));
        if (dataMission.GetInt("type") == 1) {
            string url = dataMission.GetUtfString("additional_data");
            Application.OpenURL(url);
            //Application.OpenURL("market://details?id=" + Application.productName);
        }
        GamePacket gp = new GamePacket(CommandKey.CLAIM_MISSION);
        gp.Put("id", Id);
        SFS.Instance.ClaimMissionRequest(gp);
        
        
    }
   public void CompleteReceiveGift()
    {
        btnClaim.gameObject.SetActive(false);
        TxtComplete.enabled = true;
        btnClaim.onClick.RemoveListener(onClickClaimHandler);
        AnimReceiveGift();
    }

    public void destroyMission()
    {
        Destroy(this.gameObject);
    }


    public void AnimReceiveGift()
    {
        AnimHomeController.api.playGold(MissionChip, 20, ImgGold.transform.position, HomeViewV2.api.getGoldTranform());
    }


}
