﻿using Facebook.Unity;
using MiniJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FBController : MonoBehaviour
{
    public static FBController api;
    private void Awake()
    {
        if (api != null)
        {
            Destroy(gameObject);
            return;
        }
        
        DontDestroyOnLoad(this);
        api = this;
    }

    private Action actionCompleteLoginFb = null;
    public void loginWithFbAccount(Action actionCompleteLoginFb)
    {
        //Debug.Log("init fb " + playerName);
        this.actionCompleteLoginFb = actionCompleteLoginFb;
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            InitCallback();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        FB.LogInWithReadPermissions(new List<string>() { "public_profile" }, AuthCallback);
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }


    private void AuthCallback(ILoginResult result)
    {
        
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            AccessToken aToken = AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log("login fb account successfull " + aToken.UserId);
            actionCompleteLoginFb();
          
            
        }
        else
        {
            AlertController.api.showAlert("Đăng nhập Facebook thất bại! Vui lòng thử lại");
        }
    }
}
