﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using BaseCallBack;

public class EstateControll : MonoBehaviour {

    [SerializeField]
    GameObject BtnController, PanelScroll, ScrollContent, imgAddShop_1, imgAddShop_2, imgAddShop_3, imgAddShop_4, imgAddShop_5, imgAddCar_1, imgAddCar_2, MadButtonControl;
    [SerializeField]
    MadControll Mad;
    public GameObject PanelBuyItem, PanelWorking, ObjKhungAva, ObjKhungAva_df;
    public bool View = false;
    [SerializeField]
    Image Avatar, KhungAva;

    [SerializeField]
    Text TxtTen, TxtVip, TxtChip, TxtGem, TxtPoint;
    [SerializeField]
    TestItemInHome PaneBuy;
    





    // public Dictionary<string, int> DuLieuSoHuu = new Dictionary<string, int>();


    public void InitLEFT()
    {
        TxtTen.text = MyInfo.NAME;
        long chip = MyInfo.CHIP;
        long gem = MyInfo.GEM;

        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtGem.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        TxtPoint.text = DataHelper.MY_DiemTaiSan.ToString();

        TxtVip.text = MyInfo.GetVipName();


        Avatar.sprite = MyInfo.sprAvatar;

        
        int id_Bor = MyInfo.MY_BOR_AVATAR;
        if (id_Bor != -1)
        {
            Sprite h = DataHelper.GetBoderAvatar_Shop(MyInfo.BorderAvatarName);
            if (h != null)
            {
                KhungAva.sprite = DataHelper.GetBoderAvatar_Shop(MyInfo.BorderAvatarName);
                ObjKhungAva.SetActive(true);
                ObjKhungAva_df.SetActive(false);
            }
            else
            {
                ObjKhungAva.SetActive(false);
                ObjKhungAva_df.SetActive(true);
            }

        }
        else
        {
            ObjKhungAva.SetActive(false);
            ObjKhungAva_df.SetActive(true);
        }

    }



    public void CapNhatLEFT()
    {
        long chip = MyInfo.CHIP;
        long gem = MyInfo.GEM;

        TxtChip.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        TxtGem.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        TxtPoint.text = DataHelper.MY_DiemTaiSan.ToString();
    }



    public void RequestLayDuLieuVatPham()
    {
        API.Instance.RequestLayDuLieuVatPham(RspLayDuLieuVatPham);
        
    }




    public void RequestMuaVatPham(string Id, string LoaiTien)
    {
       // Debug.LogError("sl kho truoc khi goi ===== " + DataHelper.KhoItem[Id]  + "===========Id=========" + Id);
        API.Instance.RequestMuaVatPham(Id, LoaiTien, RspLayMuaVatPham);
    }

    void RspLayMuaVatPham(string _json)
    {
        Debug.LogWarning("DU LIEU mua SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string id = node["asset_id"]["$id"].Value;


        if (sta == "1")
        {
            MyInfo.CHIP = chip;
            MyInfo.GEM = gem;



            if (DataHelper.DuLieuSoHuu.ContainsKey(id))
            {

                DataHelper.DuLieuSoHuu[id] = DataHelper.DuLieuSoHuu[id] + 1;
                Debug.LogError("sl kho truoc mua ===== " + DataHelper.KhoItem[id] + "===========Id=========" + id);
                DataHelper.KhoItem[id] = DataHelper.KhoItem[id];
                Debug.LogWarning("Da Them vao du lieu so huu CU");
            }
            else
            {

                DataHelper.DuLieuSoHuu.Add(id, 1);

                //DataHelper.KhoItem.Add(id, 1);

                if (DataHelper.KhoItem.ContainsKey(id))
                {
                    DataHelper.KhoItem[id] = 1;
                }
                else
                {
                    DataHelper.KhoItem.Add(id, 1);
                }
                
                Debug.LogWarning("Da Them moi du lieu so huu");
            }

            Debug.LogError("sl kho sau mua ===== " + DataHelper.KhoItem[id]);

            int DiemCongThem = DataHelper.DuLieuDiemTaiSan[id];
            DataHelper.MY_DiemTaiSan += DiemCongThem;
            CapNhatLEFT();

            if (DataHelper.DuLieuNha.ContainsKey(id))
            {
                Mad.ChangeHome(id);
            }
            else if (DataHelper.DuLieuShop.ContainsKey(id))
            {
                Mad.ChangeShop(id);
            }
            else if (DataHelper.DuLieuXe.ContainsKey(id))
            {
                Mad.ChangeCar(id);
            }
             
            
            PaneBuy.gameObject.SetActive(false);
        }
        else
        {
            string MSG = node["msg"].Value;
            PaneBuy.ThongBaoLoi(MSG);
        }

        Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + id);
    }


    public void RequestLayDuLieuVatPham_SoHuu()
    {
        API.Instance.RequestVatPham_SoHuu(RspLayDuLieuVatPham_SoHuu);

    }

    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("DU LIEU SAN PHAM so huu: " + _json);

        //_json = "false";

        if (_json == "false")
        {
            HienThiMadMacDinh();
            DataHelper.MY_DiemTaiSan = 0;
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);

            JSONNode data = node["assets"];

            if (data.Count == 0)
            {
                HienThiMadMacDinh();
                DataHelper.MY_DiemTaiSan = 0;
            }
            else
            {

                for (int i = 0; i < data.Count; i++)
                {
                    string id = data[i]["id"]["$id"].Value;
                    int sl = int.Parse(data[i]["qty"].Value);
                    ThemDuLieuSoHuu(id, sl);
                }

                DataHelper.KhoItem = DataHelper.DuLieuSoHuu;
                DataHelper.DuLieuMap.Clear();

                if (node["placed_assets"].Value == "null")
                {
                    HienThiMadMacDinh();
                }
                else
                {
                    int cou = node["placed_assets"].Count;
                    // Debug.LogError("BAT DC so luong NE HEHE---------- " + cou);

                    for (int i = 0; i < cou; i++)
                    {
                        int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                        string a_id = node["placed_assets"][i]["id"].Value;

                        //  Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                        DataHelper.DuLieuMap[pos] = a_id;

                        int sl = DataHelper.KhoItem[a_id];
                        sl--;
                        DataHelper.KhoItem[a_id] = sl;

                    }

                    for (int i = 0; i < 8; i++)
                    {
                        if (DataHelper.DuLieuMap.ContainsKey(i) == false)
                        {
                            DataHelper.DuLieuMap.Add(i, "");
                        }

                        //   Debug.LogError("ket vi tri ----- " + i  + " ---------- " + DataHelper.DuLieuMap[i]);
                    }

                }

            }

        }
        //  Mad.InitSpr();
        DataHelper.IsInitShopVatPham = true;
    }

    void HienThiMadMacDinh()
    {
        DataHelper.DuLieuMap.Add(0, "5dc03420e52b8e2d238b6045");
        for (int i = 1; i < 8; i++)
        {
            DataHelper.DuLieuMap.Add(i, "");
        }
    }

    void ThemDuLieuSoHuu(string id, int sl)
    {
        string key = id;
        if (DataHelper.DuLieuSoHuu.ContainsKey(key))
        {
            
            DataHelper.DuLieuSoHuu[key] = sl;
         //   Debug.LogWarning("Da Them vao du lieu so huu CU");
        }
        else
        {
            
            DataHelper.DuLieuSoHuu.Add(key, sl);
          //  Debug.LogWarning("Da Them moi du lieu so huu");
        }

        int diemVatPham = DataHelper.DuLieuDiemTaiSan[id];
        int DiemCongThem = diemVatPham * sl;
        DataHelper.MY_DiemTaiSan += DiemCongThem;
    }



    void RspLayDuLieuVatPham(string _json)
    {
        if (DataHelper.IsInitItemShop == false)
        {
            

            JSONNode node = JSONNode.Parse(_json);

            Debug.LogWarning("DU LIEU SAN PHAM: " + _json);


            JSONNode data = node["data"];

            Debug.Log(data.Count + " - " + data.ToString());


            //   http://gamebai.gamekt.club/files/assets/thumb_5dc034d15be66_h1.png
            for (int i = 0; i < data.Count; i++)
            {
                //Debug.LogError("iiii" + i);
                //for (int i = 0; i < 5; i++)        {

                string type = data[i]["type"].Value;
                string name = data[i]["name"].Value;

                string id = data[i]["id"].Value;

                string image = data[i]["image"].Value;


                long chip = long.Parse(data[i]["price"]["chip"].Value);
                long gem = long.Parse(data[i]["price"]["gem"].Value);

                long chip_save = long.Parse(data[i]["price"]["sale_chip"].Value);
                long gem_save = long.Parse(data[i]["price"]["sale_gem"].Value);


                string info = data[i]["info"].Value;
                int point = int.Parse(data[i]["asset_point"].Value);
                int stt = int.Parse(data[i]["stt"].Value);

                DataHelper.DuLieuTenImgTheoID.Add(id, image);

                DataHelper.DuLieuDiemTaiSan.Add(id, point);

                if (type == "house")
                {
                    My_Item it = new My_Item();
                    it.ten = name;
                    it.chip = chip;
                    it.chipsave = chip_save;
                    it.gem = gem;
                    it.gemsave = gem_save;
                    it.thongtin = info;
                    it.diemtaisan = point;
                    it.id = id;
                    it.img = image;
                    
                    DataHelper.DuLieuNha.Add(id, it);

                 //   API.Instance.Load_SprHouse(image, id);
                }
                else if (type == "shop")
                {
                    My_Item it = new My_Item();
                    it.ten = name;
                    it.chip = chip;
                    it.chipsave = chip_save;
                    it.gem = gem;
                    it.gemsave = gem_save;
                    it.thongtin = info;
                    it.diemtaisan = point;
                    it.id = id;
                    it.img = image;
                   
                    DataHelper.DuLieuShop.Add(id, it);

                 //   API.Instance.Load_SprShop(image, id);
                }
                else if (type == "car")
                {
                    My_Item it = new My_Item();
                    it.ten = name;
                    it.chip = chip;
                    it.chipsave = chip_save;
                    it.gem = gem;
                    it.gemsave = gem_save;
                    it.thongtin = info;
                    it.diemtaisan = point;
                    it.id = id;
                    it.img = image;
                    
                    DataHelper.DuLieuXe.Add(id, it);
                  //  API.Instance.Load_SprCar(image, id);
                }

            }

            RequestLayDuLieuVatPham_SoHuu();
            DataHelper.IsInitItemShop = true;
        }
        
    }







    public void ffff()
    {
        Debug.LogWarning("SO NHA--------------- " + DataHelper.DuLieuNha.Count);
        Debug.LogWarning("SO shop--------------- " + DataHelper.DuLieuShop.Count);
        Debug.LogWarning("SO xe--------------- " + DataHelper.DuLieuXe.Count);
    }

    public void BtnClick(int type)
    {
        Debug.Log("DangClick");
        BtnController.SetActive(false);
        PanelScroll.SetActive(true);
        if (type == 1)
        {
            PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemHomeCroll();
        }
    }

    public void Btn_Shop_1_Click(int type)
    {
        BtnController.SetActive(false);
        PanelScroll.SetActive(true);
        PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemShopInScoll();
        Mad.ID_ShopToChange = type;      
    }

    public void Btn_Car_Click(int type)
    {
        BtnController.SetActive(false);
        PanelScroll.SetActive(true);
        PanelScroll.transform.GetChild(0).GetComponent<ItemScroll>().InitItemCarInScoll();
        Mad.ID_CarToChange = type;
    }


    public void CloseItemScroll()
    {
        PanelScroll.SetActive(false);
        OpenBtnController();
    }
    public void OpenItemScroll()
    {
        PanelScroll.SetActive(true);
    }

    public void OpenBtnController()
    {
        BtnController.SetActive(true);
    }

    public void CloseBtnController()
    {
        BtnController.SetActive(false);
    }

    public void ClosePanelBuyItem()
    {
        PanelBuyItem.SetActive(false);
    }

    public void OpenPanelBuyItem()
    {
        PanelBuyItem.SetActive(true);
    }

    public void OpenPanelWorking()
    {
        PanelWorking.SetActive(true);
    }

    public void ClosePanelWorking()
    {
        PanelWorking.SetActive(false);
    }

    public void IsVisitors()
    {
        imgAddShop_1.SetActive(false);
        imgAddShop_2.SetActive(false);
        imgAddShop_3.SetActive(false);
        imgAddShop_4.SetActive(false);
        imgAddShop_5.SetActive(false);

        imgAddCar_1.SetActive(false);
        imgAddCar_2.SetActive(false);

        MadButtonControl.SetActive(false);
    }

    public void BtnBackOnClick()
    {
        Mad.BackAndSave();
        this.gameObject.SetActive(false);
    }
}




public class My_Item : MonoBehaviour
{

    public string ten, thongtin, id, img, soluong;
    public long chip, chipsave, gem, gemsave;
    public int diemtaisan;
}

