﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScroll : MonoBehaviour {
    [SerializeField]
    GameObject Content, ItemHome, ItemShop, ItemCar;
    [SerializeField]
    MadControll Mad;
    Dictionary<string, Sprite> ListSpriteItemHome;
    Dictionary<string, Sprite> ListSpriteItemShop;
    Dictionary<string, Sprite> ListSpriteItemCar;
    Dictionary<string, GameObject> DicHomeItem;
    Dictionary<string, GameObject> DicShopItem;
    Dictionary<string, GameObject> DicCarItem;

    [SerializeField]
    EstateControll Estate;

    public void InitContent(float left, float right)
    {
        //Content.GetComponent<RectTransform>().
    }



    public void UpdateKhoItem()
    {
        for (int i = 0; i < 8; i++)
        {
            if (DataHelper.DuLieuMap[i] != null)
            {
                string id = DataHelper.DuLieuMap[i];
                if (DataHelper.KhoItem[id] != null)
                {
                    DataHelper.KhoItem[id] = DataHelper.KhoItem[id] - 1;
                }
               
            }

        }
    }


    public void InitItemHomeCroll()
    {
        
        
        
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }

        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }


        
        DicHomeItem = new Dictionary<string, GameObject>();

       


        foreach (var item in DataHelper.DuLieuNha)
        {
            if (item.Key != "5dc03420e52b8e2d238b6045")
            {
                if (DataHelper.KhoItem.ContainsKey(item.Key) == true && DataHelper.KhoItem[item.Key] > 0)
                {
                    Debug.LogError("DataHelper.KhoItem[Id]==check=== " + DataHelper.KhoItem[item.Key]);
                    InitItemHome(item.Key, DataHelper.DuLieuSoHuu[item.Key].ToString(), false);
                }
                else
                {
                    InitItemHome(item.Key, "0");
                }
            }
                        
        }

        
    }


    public void InitItemShopInScoll()
    {
        ListSpriteItemShop = DataHelper.dictSprite_Shop;
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }

        DicShopItem = new Dictionary<string, GameObject>();

     //   UpdateKhoItem();

        foreach (var item in DataHelper.DuLieuShop)
        {
            if (DataHelper.KhoItem.ContainsKey(item.Key) == true && DataHelper.KhoItem[item.Key] > 0)
            {
                InitItemShop(item.Key, DataHelper.DuLieuSoHuu[item.Key].ToString(), false);
            }
            else
            {
                InitItemShop(item.Key, "0");
            }

        }


    }



    public void InitItemCarInScoll()
    {
        ListSpriteItemCar = DataHelper.dictSprite_Car;
        if (DicHomeItem != null)
        {
            foreach (var item in DicHomeItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicShopItem != null)
        {
            foreach (var item in DicShopItem)
            {
                Destroy(item.Value);
            }
        }
        if (DicCarItem != null)
        {
            foreach (var item in DicCarItem)
            {
                Destroy(item.Value);
            }
        }


        DicCarItem = new Dictionary<string, GameObject>();

        foreach (var item in DataHelper.DuLieuXe)
        {
            if (DataHelper.KhoItem.ContainsKey(item.Key) == true && DataHelper.KhoItem[item.Key] > 0)
            {
                InitItemCar(item.Key, DataHelper.DuLieuSoHuu[item.Key].ToString(), false);
            }
            else
            {
                InitItemCar(item.Key, "0");
            }
        }
    }










    public void InitItemHome(string Id, string count, bool clock = true)
    {
        GameObject Obj = Instantiate(ItemHome) as GameObject;
        // Obj.transform.SetParent(Content.transform);
        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
 
        ItemShopHome MyItem = Obj.GetComponent<ItemShopHome>();


        My_Item it = DataHelper.DuLieuNha[Id];



     //   Sprite spr = DataHelper.dictSprite_House[Id];


        MyItem.Init(Id, clock, count);
 
        DicHomeItem.Add(Id, Obj);
        
    }

    public void InitItemShop(string Id, string count, bool clock = true )
    {
        GameObject Obj = Instantiate(ItemShop) as GameObject;

        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        
        ItemShop MyItem = Obj.GetComponent<ItemShop>();

       
        My_Item it = DataHelper.DuLieuShop[Id];
        

       // Sprite hinh = ListSpriteItemShop[Id];
        string ten = it.ten;
        long gem = it.gem;
        long chip = it.chip;
        int diem = it.diemtaisan;
        string thongtin = it.thongtin;


        MyItem.Init(Id, clock, count);

       
        DicShopItem.Add(Id, Obj);
       
    }

    public void InitItemCar(string Id, string count, bool clock = true)
    {     
        GameObject Obj = Instantiate(ItemCar) as GameObject;
        // Obj.transform.SetParent(Content.transform);
        Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
        Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        
        ItemShop MyItem = Obj.GetComponent<ItemShop>();

        
        My_Item it = DataHelper.DuLieuXe[Id];

        MyItem.InitCar(Id, clock, count);
    
        DicCarItem.Add(Id, Obj);
    }

}
