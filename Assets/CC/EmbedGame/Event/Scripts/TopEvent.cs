﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class TopEvent : MonoBehaviour {
    public GameObject RowPrefab;
    public GameObject Container;
    public ArrayList contents;
    void Start () {
        contents = new ArrayList();
	}
	
    public void Open(GamePacket gp)
    {
        gameObject.SetActive(true);
        GetSicboTop(gp);
        //GamePacket gp = new GamePacket(LuckyMiniCommandKey.GetSicboTop);
        //SFS.Instance.SendRoomRequest(gp);      
    }

    //public void onExtensionResponse(GamePacket gp)
    //{
    //    switch (gp.cmd)
    //    {
    //        case LuckyMiniCommandKey.GetSicboTop:
    //            GetSicboTop(gp);
    //            break;
    //    }
    //}

    private void GetSicboTop(GamePacket gp)
    {
        var logTx = gp.GetSFSArray("top");//gp.param.GetSFSArray(LuckyMiniParamKey.TopSicbo);
        Debug.Log("logTx "+ logTx.Size());
        for (int i = 0; i < Container.transform.childCount; i++)
        {
            var gameObj = Container.transform.GetChild(i).gameObject;            
            gameObj.SetActive(false);
        }
        if (logTx != null)
        {
            if (logTx.Count > Container.transform.childCount)
            {
                int diff = logTx.Count - Container.transform.childCount;
                GameObject rowObj = null;
                for (int i = 0; i < diff; i++)
                {
                    rowObj = Instantiate(RowPrefab);
                    rowObj.transform.SetParent(Container.transform);
                    //rowObj.transform.parent = Container.transform;
                    rowObj.transform.localScale = new Vector3(1, 1, 1);
                    rowObj.transform.localPosition = Vector3.zero;
                }
            }
            for (int i = 0; i < logTx.Count; i++)
            {
                GameObject rowObj = Container.transform.GetChild(i).gameObject;
                rowObj.SetActive(true);
                var content = rowObj.GetComponent<TopEventContent>();
                content.Show(i + 1, (SFSObject)logTx.GetSFSObject(i));
            }
            
        }
    }

    public void Close()
    {
        gameObject.SetActive(false);
        for (int i = 0; i < Container.transform.childCount; i++)
        {
            contents.Add(Container.transform.GetChild(i).gameObject);
        }
    }
}
