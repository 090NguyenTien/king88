﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class LuckyHistoryContent : MonoBehaviour {
    public Text Order;
    public Text Time;
    public Text Betting;
    public Text Result;
    public Text BetMoney;
    public Text Return;
    public Text Receive;
    public GameObject RowBg;
    public void Show(int index, SFSObject data)
    {
        //{ "choice": "tai", "type_money": "chip", "bet_money": 100, "return_money": 100, "gain_money": 200, "dice": "2,5,6", "room_id": 270716111538, "created_date": "2016-22-10 19:00:00" }
		Order.text = data.GetUtfString("room_id");
		Time.text = data.GetUtfString("created_date");
        //Debug.LogError("time " + data.GetUtfString("created_date"));
        string bettype = data.GetUtfString("choice");
        Betting.text = bettype.Equals("Sic") ? "Tài" : "Xỉu";
        Result.text = data.GetUtfString("dice");
		BetMoney.text = "$"+data.GetLong("bet_money").ToString("N0");
		Return.text = "$"+data.GetLong("return_money").ToString("N0");
		Receive.text = "$"+data.GetLong("gain_money").ToString("N0");
        RowBg.SetActive(index % 2 == 0);
    }
}
