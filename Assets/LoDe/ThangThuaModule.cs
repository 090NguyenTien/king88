﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThangThuaModule : MonoBehaviour {

    public Text KetQuaTxt, SoDanhTxt, TypeTxt, TienDatTxt, TienThangTxt, SoLanXuatHienTxt;
    public bool Win;
    private Image ImgFace;
    private DataItemBetToDay item;
    
    [SerializeField]
    Button BtnThangThua;
    
    private ResultLotteryManager KetQua;

    public void Init(DataItemBetToDay item)
    {
        this.item = item;
        ImgFace = gameObject.transform.GetChild(0).gameObject.GetComponent<Image>();

        KetQua = GameObject.Find("Lottery").GetComponent<ResultLotteryManager>();

        
        BtnThangThua.onClick.RemoveAllListeners();
        BtnThangThua.onClick.AddListener(BtnThangThuaOnClick);


    }


    public void BtnThangThuaOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        if (Win == true)
        {
            KetQua.ShowResultDefault();
            int type = GetIndexByStringType(TypeTxt.text);

            Debug.Log("So Danh -  " + SoDanhTxt.text);
            Debug.Log("Loai -  " + type);
            KetQua.OffPanelBetResult();
            KetQua.TimSoTrung(SoDanhTxt.text, type);
        }
    }


    private int GetIndexByStringType(string Type)
    {
        int index = 0;

        if (Type == "LÔ 3 SỐ")
        {
            index = 1;
        }
        else if (Type == "3 CÀNG")
        {
            index = 2;
        }
        else if (Type == "ĐỀ ĐẦU")
        {
            index = 3;
        }
        else if (Type == "ĐỀ ĐẶC BIỆT")
        {
            index = 4;
        }
        else if (Type == "ĐÁNH ĐẦU")
        {
            index = 5;
        }
        else if (Type == "ĐÁNH ĐUÔI")
        {
            index = 6;
        }

        return index;
    }



    public void ShowAfterHaveResult(bool isWin, string number, string type,  string money, string moneywin, string count)
    {
        Win = isWin;
        if (Win == true)
        {
            KetQuaTxt.text = "Thắng";
            SoDanhTxt.text = number;
            TypeTxt.text = type;
            TienDatTxt.text = Utilities.GetStringMoneyByLong(long.Parse(money));
            TienThangTxt.text = Utilities.GetStringMoneyByLong(long.Parse(moneywin));
            SoLanXuatHienTxt.text = count;
        }
        else
        {
            KetQuaTxt.text = "<color=#8E9292FF>Thua</color>";
            SoDanhTxt.text = "<color=#8E9292FF>" + number + "</color>";
            TypeTxt.text = "<color=#8E9292FF>" + type + "</color>";
            string mon = Utilities.GetStringMoneyByLong(long.Parse(money));
            string mwin = Utilities.GetStringMoneyByLong(long.Parse(moneywin));
            TienDatTxt.text = "<color=#8E9292FF>" + mon + "</color>";
            TienThangTxt.text = "<color=#8E9292FF>" + mwin + "</color>";
            SoLanXuatHienTxt.text = "<color=#8E9292FF>" + count + "</color>";
            BtnThangThua.GetComponent<Image>().color = Color.gray;
        }
    }

    public void ShowBeforHaveResult(string number, string type, string money)
    {
        KetQuaTxt.text = "?";
        SoDanhTxt.text = number;
        TypeTxt.text = type;
        TienDatTxt.text = Utilities.GetStringMoneyByLong(long.Parse(money));
        TienThangTxt.text = "?";
        SoLanXuatHienTxt.text = "?";
        BtnThangThua.gameObject.SetActive(false);
    }



}
