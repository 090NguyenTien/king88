﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryModule : MonoBehaviour {

    
    public Text DateTxt, TypeTxt, BetNumBerTxt, MoneyTxt, MoneyWinTxt;

    private HistoryItemHome item;

    public void Init(HistoryItemHome item)
    {
        this.item = item;
    }
   
    public void Show(string date, string type, string number, string money, string moneywin)
    {
        DateTxt.text = date;
        TypeTxt.text = type;
        BetNumBerTxt.text = number;
        MoneyTxt.text = Utilities.GetStringMoneyByLong(long.Parse(money));
        int intMoneyWin = int.Parse(moneywin);
        if (intMoneyWin < 0)
        {
            MoneyWinTxt.text = "0";
        }
        else
        {
            MoneyWinTxt.text = Utilities.GetStringMoneyByLong(long.Parse(moneywin));
        }
    }
}
