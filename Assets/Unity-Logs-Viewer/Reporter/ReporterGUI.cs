﻿using UnityEngine;
using System.Collections;

public class ReporterGUI : MonoBehaviour
{
	Reporter2 reporter;
	void Awake()
	{
		reporter = gameObject.GetComponent<Reporter2>();
	}

	void OnGUI()
	{
		reporter.OnGUIDraw();
	}
}
